import java.io.* ;
import java.net.* ;
import java.util.* ;

public class UDPClientApp{
	public static void main(String argv[]) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		byte[] toSend = new byte[256];
		byte[] sReceive = new byte[256];
		int port = 2407;
		String cadenaMensaje="";  
		try {
			DatagramSocket socket = new DatagramSocket();
			InetAddress address=InetAddress.getByName("localhost");
			while(true) {
				String mensaje = in.readLine();
				toSend = mensaje.getBytes();
				DatagramPacket packet = new DatagramPacket(toSend,mensaje.length(),address,port);
				socket.send(packet); 
				DatagramPacket sPacket = new DatagramPacket(sReceive,256);
				socket.receive(sPacket);
				cadenaMensaje = new String(sReceive).trim();
				System.out.println(cadenaMensaje);
			}
		 }
		catch (Exception e) {
		 	System.err.println(e.getMessage());
			System.exit(1);
		}
 	}
} 