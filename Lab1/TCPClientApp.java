import java.io.* ;
import java.net.* ;
import java.util.* ;

public class TCPClientApp{
	public static void main(String argv[]) {
		int port = 2407;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		Socket socket;
		try {
			socket = new Socket("localhost",port);
			System.out.println("You can type to the server...");
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			DataInputStream received = new DataInputStream(socket.getInputStream());
			while(true){
				String mensaje = in.readLine();
				out.writeUTF(mensaje);
				String receive = received.readUTF();
				System.out.println(receive);
			}
		 }
		catch (Exception e) {
		 	System.err.println(e.getMessage());
		 	System.exit(1);
		}
	}
} 