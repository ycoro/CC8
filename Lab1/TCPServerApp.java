import java.io.* ;
import java.net.* ;
import java.util.* ;

public class TCPServerApp{
	 public static void main(String argv[]) {
	 	int port = 2407;
		ServerSocket socket;
		try {
			socket = new ServerSocket(port);
			Socket client = socket.accept();
			System.out.println("Waiting for client to write...");
			DataInputStream in = new DataInputStream(client.getInputStream());
			DataOutputStream out = new DataOutputStream(client.getOutputStream());
			while(true){
				String mensaje ="";
				mensaje = in.readUTF();
				System.out.println(mensaje);
				out.writeUTF(mensaje.toUpperCase());
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
}