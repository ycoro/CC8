import java.io.* ;
import java.net.* ;
import java.util.* ;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class WebServer{

//port 2407
  public static void main(String[] args) throws Exception{
  	String test = openFile("config.txt");
  	int nThreads = regularExpression(test);
  	ExecutorService executor = Executors.newFixedThreadPool(nThreads);
 	int port = 2407;
  	try {
	    ServerSocket server = new ServerSocket(port);
	    Socket connection = null;
	    System.out.println("Welcome to YCoro server... Listening on port: " + port);
	    System.out.println("Press ctrl + C to quit.");
	    System.out.println("Waiting for connection...");
	    while(true){
	    	connection = server.accept();
	      	Http pool = new Http(connection);
	      	executor.execute(pool);
	      }
	    }
	    catch (Exception e) { 
	    	System.out.println("Can't connect to server");
	    	System.exit(1);
	    }
  }
  public static String openFile(String path){
  	String content = null;
    File file = new File(path); //for ex foo.txt
    FileReader reader = null;
    try {
        reader = new FileReader(file);
        char[] chars = new char[(int) file.length()];
        reader.read(chars);
        content = new String(chars);
        reader.close();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return content;
  }
  public static int regularExpression(String match){
  	int r = 0;
  	Pattern pattern = Pattern.compile("([a-zA-Z])+=<([0-9]*[1-9]+)>");
  	Matcher matcher = pattern.matcher(match);
  	boolean matches = matcher.matches();
  	if(matches){
  		r = Integer.parseInt(matcher.group(2));
  	}else{
        System.out.println("Invalid config.txt file... please fix it.\n Should look like this: MaxThreads=<numberOfThreads>");
    }
        return r;
  }
}
  final class Http implements Runnable{
  	Socket connection = null;
  		public Http(Socket socket) {
    		this.connection = socket;
   		}
	  public void run() {
	      //System.out.println("Connected in..."+ this.socket);
	      try {
	        BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        //response header + file  
	        DataOutputStream output = new DataOutputStream(connection.getOutputStream());
	        //System.out.println("Listening on port: " + connection.getLocalPort());
	        httpRequests(input, output);
	      }
	      catch (Exception e) {
	        System.out.println("Buffers cannot be placed.");
	      }
	  }
	  private void httpRequests(BufferedReader input, DataOutputStream output) {
	    int requestType = 0;
	    String header = ""; 
	    String url = ""; 
	    String text = null;
	    try {
	      //GET /index.html HTTP/1.1
	      //HEAD /index.html HTTP/1.1
	      String a = input.readLine();
	      String a1 = new String(a);
	      while((text=input.readLine())!=null && !text.equals("")){
	  	  	header = header + text + "\n";
	  	  }
	  	  System.out.println(header);
	      a.toUpperCase(); 
	      if (a.startsWith("GET")) { 
	        requestType = 1;
	      } 
	      if (a.startsWith("HEAD")) { 
	        requestType = 2;
	      } 
	      if (requestType == 0) {
	        try {
	          output.writeBytes(httpHeader(501, 0));
	          output.close();
	          return; //Return to get another request.
	        }
	        catch (Exception e) { 
	        	System.out.println("something went wrong :(....");
	      	}//Get the text after Get/...
	      }
	      int start = 0;
	      int end = 0;
	      for (int letter = 0; letter < a1.length(); letter++) {
	        if (a1.charAt(letter) == ' ' && start != 0) {
	          end = letter;
	          break;
	        }
	        if (a1.charAt(letter) == ' ' && start == 0) {
	          start = letter;
	        }
	      }
	      url = a1.substring(start + 2, end); //get the left spaces. removes the /
	      if(url.equals(""))url = "index.html";//If it's empty i set it to index.html
	      System.out.println("Opening:  " + url);
		}
	    catch (Exception e) {
	      System.out.println("Can't information. Parser went wrong.");
	    } 

	    FileInputStream reqFile = null;

	    try {//Just passed the url...that we got before.
	      File exist = new File(url);
	      if(exist.exists()){
	      	reqFile = new FileInputStream(url);
	      }
	      else{
	      	reqFile = new FileInputStream("404.html");
	      }
	    }
	    catch (Exception e) {
	      try {
	        output.writeBytes(httpHeader(404, 5));//Not Found
	        output.close();
	      }
	      catch (Exception e2) {
	      	System.out.println("Cant process 404.");
	      }
	    } 
	    try {
	      int fileType = 5;
	      if (url.endsWith(".zip")){
	        fileType = 1;
	      }
	      if (url.endsWith(".jpg") || url.endsWith(".jpeg")) {
	        fileType = 2;
	      }
	      if (url.endsWith(".gif")) {
	        fileType = 3;
	      }
	      output.writeBytes(httpHeader(200, fileType));
	      if (requestType == 1) { 
	        while (true) {
	          int b = reqFile.read();
	          if (b == -1) {
	            break;
	          }
	          output.write(b);
	        }
	        
	      }
	//Clean crew.
	      output.close();
	      reqFile.close();
	      //System.out.println(header);
	    }

	    catch (Exception e) {

	    }

	  }

	  private String httpHeader(int statusCode, int file) {
	    String r = "HTTP/1.1 ";
	    switch (statusCode) {
	      case 200:
	        r = r + "200 OK";
	        break;
	      case 400:
	        r = r + "400 Bad Request";
	        break;
	      case 403:
	        r = r + "403 Forbidden";
	        break;
	      case 404:
	        r = r + "404 Not Found";
	        break;
	      case 500:
	        r = r + "500 Internal Server Error";
	        break;
	       case 501:
	       	r = r + "501 Not Implemented.";
	       	break;
	    }

	    r = r + "\r\n"; 
	    r = r + "Date: Mon, 27 Jul 2015 12:00:15 GMT\r\n";
	    r = r + "Connection: close\r\n"; 
	    r = r + "Server: Lab02-12003604\r\n"; 
	    switch (file) {
	      case 0: // i dont want to say anything.
	        break;
	      case 2:
	        r = r + "Content-Type: image/jpeg\r\n";
	        break;
	      case 3:
	        r = r + "Content-Type: image/gif\r\n";
	      case 1:
	        r = r + "Content-Type: application/x-zip-compressed\r\n";
	      default:
	        r = r + "Content-Type: text/html\r\n";
	        break;
	    }
	    r = r + "\r\n"; //EOF
	    return r;
	  }
	}