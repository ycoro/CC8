import java.io.* ;
import java.net.* ;
import java.util.* ;

public class UDPServerApp{
	public static void main(String argv[]) {
 		int port = 2407;
		boolean fin = false; 
		byte[] response = new byte[256];
		try {
			DatagramSocket socket = new DatagramSocket(port);
			byte[] message = new byte[256];
			String m ="";
			InetAddress address;
			while(true) {
				DatagramPacket packet = new DatagramPacket(message,256);
				socket.receive(packet);
				m = new String(message).trim();
				System.out.println(m);
				address = packet.getAddress(); 
				port = packet.getPort();
				m = m.toUpperCase();
				response = m.getBytes();
				DatagramPacket toSend = new DatagramPacket(response,m.length(),address,port);
				socket.send(toSend);
			}
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
 	}
} 